export function initMedia() {
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    console.log('getUserMedia supported.');
    return navigator.mediaDevices.getUserMedia({audio: true}).then(stream => {
      return new MediaRecorder(stream)
    })
      .catch(function (err) {
          console.log('The following getUserMedia error occured: ' + err);
        }
      );
  } else {
    console.log('getUserMedia not supported on your browser!');
  }

}

